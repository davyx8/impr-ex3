function imBlend = pyramidBlending(im1, im2, mask, maxLevels, filterSizeIm, filterSizeMask)
 [pyr1 fil1] = LaplacianPyramid(im1, maxLevels,filterSizeIm );
 [pyr2 fil2] = LaplacianPyramid(im2, maxLevels,filterSizeIm );
 [maskPyr ~] =GaussianPyramid(mask, maxLevels, filterSizeMask );
 lapPyr = gadd(gmultiply(pyr1 , maskPyr), gmultiply(gsubtract(1,maskPyr) , pyr2));
 imBlend=LaplacianToImage(lapPyr,fil1,ones(length(pyr1),1));   
end