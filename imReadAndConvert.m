function im = imReadAndConvert(filename, representation)
im = im2double(imread(filename)) ;
info= imfinfo(filename);
type=info(1).ColorType;
    switch (representation)
        case(1) 
       if (strcmp(type, 'truecolor' ))
             im=im2double(rgb2gray(im));
           end
       case (2) 
         if (strcmp(type, 'grayscale' ))
         im=im2double(im);
         end   
        otherwise
        display('wrong usage please choose 1 or 2');
        return;
    end
return; 
end