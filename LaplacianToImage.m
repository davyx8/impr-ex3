function img = LaplacianToImage(lpyr, filter, coeffMultVec)
im=lpyr{end};
filter=filter*2;

for i=drange(numel(coeffMultVec)-1:-1:1)
    x=lpyr{i}.*coeffMultVec(i);
    [w,h]=size(lpyr{i});
    exp=zeros(w,h);
    exp(1:2:end,1:2:end)=im;
    exp = imfilter(exp,filter,'symmetric');
    exp = imfilter(exp,filter','symmetric');
    im = minus(exp,x);
end
img=im;

end