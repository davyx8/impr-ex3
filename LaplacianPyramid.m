function [pyr, filter] = LaplacianPyramid(im, maxLevels, filterSize)

    curr = im;
    
    [w,h] = size(curr);
    levels = ceil(log2(min(w,h)/16));
    levels = min(levels,maxLevels);
    
    pyr = cell(levels,1);
    pyr{1} = curr;
    
   [gauPyr, filter] = GaussianPyramid(im, levels, filterSize);
   filter2 = filter*2;
    for i=1:levels-1    
        itr=gauPyr{i+1}; 
        plane = zeros(size(gauPyr{i}));
        plane(1:2:end,1:2:end) = itr;
        plane = imfilter(plane,filter2,'symmetric');
        plane = imfilter(plane,filter2','symmetric');
        pyr{i} = minus(plane,gauPyr{i});
    end
    
     pyr{levels} = gauPyr{levels};
        
end
