function res = renderPyramid(pyr,levels)
res=[];
for i=drange(1:levels)
tmp=pyr{i};
[w h]=size(tmp);
[a b]=size(res);
if i~=1
    tmp=padarray(tmp,a-w,1,'post');
end;

res=[res tmp];
end

end